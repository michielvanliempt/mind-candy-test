package com.michielvanliempt.mindcandytest;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.GridView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import de.greenrobot.event.EventBus;


public class MainActivity extends Activity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private GridView gridView;
    private PhotoFeed feed;
    private FeedAdapter adapter;

    public void onEventMainThread(FeedChangedEvent event)
    {
        if (event.getFeed() == feed && feed.getOperation() == null) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initImageLoader();
        EventBus.getDefault().register(this);

        setContentView(R.layout.activity_main);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_view);
        gridView = (GridView) swipeRefreshLayout.findViewById(R.id.grid_view);

        feed = new PhotoFeed();
        FlickrApi.loadFeed(feed);
        adapter = new FeedAdapter(this, feed);
        EventBus.getDefault().register(adapter);

        gridView.setAdapter(adapter);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount == 0) {
                    return;
                }

                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    // we're at the end of list, load next page if needed
                    FlickrApi.loadMoreFeed(feed);
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                FlickrApi.loadFeed(feed);
            }
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(adapter);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                        .resetViewBeforeLoading(true)
                        .showImageOnLoading(0)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .displayer(new FadeInBitmapDisplayer(400, true, false, false))
                        .build())
                .memoryCacheSizePercentage(5)
                .build();
        ImageLoader.getInstance().init(config);
    }
}
