package com.michielvanliempt.mindcandytest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michielvanliempt on 05/11/14.
 */
public class PhotoFeed {
    private final FlickrApi api;
    List<Photo> photos = new ArrayList<Photo>();
    private int size;
    private FeedTask operation;

    public PhotoFeed() {
        api = new FlickrApi();
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public int size() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void cancelOperation() {
        if (operation != null) {
            operation.cancel(false); //TODO think a bit more about whether we can interrupt the operation
        }
    }

    public void setOperation(FeedTask operation) {
        this.operation = operation;
    }

    public FeedTask getOperation() {
        return operation;
    }
}
