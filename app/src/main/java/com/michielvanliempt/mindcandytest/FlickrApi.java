package com.michielvanliempt.mindcandytest;

import android.os.AsyncTask;

import org.springframework.web.client.RestTemplate;

import de.greenrobot.event.EventBus;

/**
 * Created by michielvanliempt on 04/11/14.
 */
public class FlickrApi {
    private static final String KEY = "f1fbef6d794e3a1df2972304eca1309b";
    private static final String SECRET = "65825c693b795400";

    private static final int PAGE_SIZE = 20;
    private static final String LOAD_CALL = "https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&api_key=" + KEY + "&per_page=" + PAGE_SIZE + "&format=json&nojsoncallback=1";
    private static final RestTemplate rest = new RestTemplate();

    public static void loadFeed(PhotoFeed feed) {
        feed.cancelOperation();
        FeedTask task = new FeedTask() {
            @Override
            protected Void doInBackground(PhotoFeed... params) {
                for (PhotoFeed feed : params) {
                    Result result = rest.getForObject(LOAD_CALL, Result.class);
                    if (isValid(result)) {
                        Photos data = result.getPhotos();
                        feed.setSize(data.getTotal());
                        feed.getPhotos().clear();
                        feed.getPhotos().addAll(data.getPhoto());
                        feed.setOperation(null);
                        EventBus.getDefault().post(new FeedChangedEvent(feed));
                    }
                    else {
                        feed.setOperation(null);
                    }
                }
                return null;
            }
        };
        feed.setOperation(task);
        task.execute(feed);
    }

    public static void loadMoreFeed(PhotoFeed feed) {
        if (feed.getOperation() != null) {
            // only one loading operation at a time
            return;
        }

        FeedTask task = new FeedTask() {
            @Override
            protected Void doInBackground(PhotoFeed... params) {
                for (PhotoFeed feed : params) {
                    int numLoaded = feed.getPhotos().size();
                    if (numLoaded >= feed.size()) {
                        // already loaded everything there is to load
                        continue;
                    }
                    int page = 1 + (numLoaded / PAGE_SIZE);
                    String call = LOAD_CALL + "&page=" + page;
                    Result result = rest.getForObject(call, Result.class);
                    if (isValid(result)) {
                        Photos data = result.getPhotos();
                        feed.setSize(data.getTotal());
                        feed.getPhotos().addAll(result.getPhotos().getPhoto());
                        feed.setOperation(null);
                        EventBus.getDefault().post(new FeedChangedEvent(feed));
                    }
                    else {
                        feed.setOperation(null);
                    }
                }
                return null;
            }
        };
        feed.setOperation(task);
        EventBus.getDefault().post(new FeedChangedEvent(feed)); // the view will display place holders while loading
        task.execute(feed);
    }

    private static boolean isValid(Result result) {
        return result != null &&
                "ok".equals(result.getStat()) &&
                result.getPhotos() != null &&
                result.getPhotos().getPhoto() != null;
    }
}
