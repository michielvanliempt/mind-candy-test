package com.michielvanliempt.mindcandytest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by michielvanliempt on 05/11/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Photo {
    private String id;
    private String farm;
    private String secret;
    private String server;
    private String title;

    public String getId() {
        return id;
    }

    public String getServer() {
        return server;
    }
    public String getFarm() {
        return farm;
    }

    public String getSecret() {
        return secret;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return String.format("http://farm%s.staticflickr.com/%s/%s_%s_q.jpg", farm, server, id, secret);
    }
}
