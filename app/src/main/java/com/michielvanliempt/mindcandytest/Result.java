package com.michielvanliempt.mindcandytest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by michielvanliempt on 04/11/14.
 */
public class Result {
    private Photos photos;
    private String stat;

    public String getStat() {
        return stat;
    }

    public Photos getPhotos() {
        return photos;
    }
}
