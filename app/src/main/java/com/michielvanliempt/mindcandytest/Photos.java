package com.michielvanliempt.mindcandytest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by michielvanliempt on 05/11/14.
 */
public class Photos {
    private String page;
    private String pages;
    private String perpage;
    private int total;
    private ArrayList<Photo> photo;

    public String getPage() {
        return page;
    }

    public String getPages() {
        return pages;
    }

    public String getPerpage() {
        return perpage;
    }

    public int getTotal() {
        return total;
    }

    public ArrayList<Photo> getPhoto() {
        return photo;
    }
}
