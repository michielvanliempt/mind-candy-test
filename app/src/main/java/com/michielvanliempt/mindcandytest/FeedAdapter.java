package com.michielvanliempt.mindcandytest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by michielvanliempt on 04/11/14.
 */
public class FeedAdapter extends BaseAdapter {

    private ImageLoader imageLoader = ImageLoader.getInstance();

    private final LayoutInflater inflater;
    private final PhotoFeed feed;

    public FeedAdapter(Context context, PhotoFeed feed) {
        inflater = LayoutInflater.from(context);
        this.feed = feed;
    }

    public void onEventMainThread(FeedChangedEvent event)
    {
        if (event.getFeed() == feed) {
            notifyDataSetChanged();
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < feed.getPhotos().size()){
            return 0;
        }
        return 1;
    }

    @Override
    public int getCount() {
        int size = feed.getPhotos().size();
        if (feed.getOperation() instanceof FeedTask) {
            return size + 1;
        }
        return size;
    }

    @Override
    public Photo getItem(int position) {
        if (position >= feed.getPhotos().size()) {
            return null;
        }
        return feed.getPhotos().get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == 0) {
            return getPhotoView(position, convertView, parent);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_tile_loading, parent, false);
        }
        return convertView;
    }

    private View getPhotoView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.grid_tile, parent, false);
        }
        Photo photo = getItem(position);
        // while loading we have place holders without data
        if (photo == null) {
            TextView userName = (TextView) convertView.findViewById(R.id.user_name);
            userName.setText("loading...");
        } else {
            TextView userName = (TextView) convertView.findViewById(R.id.user_name);
            userName.setText(photo.getTitle());

            TextView favs = (TextView) convertView.findViewById(R.id.favs);
            favs.setText(photo.getId());

            imageLoader.displayImage(photo.getUrl(), (ImageView) convertView.findViewById(R.id.photo));
        }

        return convertView;
    }
}
