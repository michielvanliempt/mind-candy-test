package com.michielvanliempt.mindcandytest;

/**
 * Created by michielvanliempt on 04/11/14.
 */
public class FeedChangedEvent {
    private PhotoFeed feed;

    public FeedChangedEvent(PhotoFeed feed) {
        this.feed = feed;
    }

    public PhotoFeed getFeed() {
        return feed;
    }

}
